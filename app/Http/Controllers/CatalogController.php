<?php

namespace App\Http\Controllers;


use App\Entities\Catalog;
use App\Http\Controllers\Controller;
use App\Infrastructure\Catalog\DoctrineCatalogRepository;
use App\Repositories\CatalogRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;

class CatalogController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Catalog Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles catalogs
    |
    */
    private $catalogs;

    public function __construct(CatalogRepository $catalogs)
    {
        $this->catalogs = $catalogs;
    }

    /**
     * Display a listing of the catalogs
     *
     * @return Response
     */
    public function index()
    {
        $catalogs = $this->catalogs->findAll();
        return response()->json(["catalogs"=>$catalogs],200);
    }

    /**
     * Store a new catalog
     *
     * @param  Request  $request
     * @return Response
     */
    protected function store(Request $request)
    {
        $this->validate($request, [
            'text' => 'required'
        ]);

        $parent = null;

        if($request->input('parent')!="#"){
            $parent = $this->catalogs->find(intval($request->input('parent')));
        }

        $catalog = $this->catalogs->create([
            'name' => $request->input('text'),
            'parent' => $parent
        ]);

        $this->catalogs->save($catalog);

        return response()->json(["catalog" => $catalog],200);
    }

    /**
     * Update the specified catalog.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'text' => 'required',
            'parent' => 'required',
            'id' => 'required'
        ]);

        $parent = null;
        if($request->input('parent')!="#"){
            $parent = $this->catalogs->find(intval($request->input('parent')));
        }

        $catalog = $this->catalogs->update([
            'name' => $request->input('text'),
            'parent' => $parent
        ],intval($request->input('id')));

        $this->catalogs->save($catalog);

        return response()->json(["catalog" => $catalog],200);
    }


    /**
     * Delete the specified catalog.
     *
     * @param  Request  $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id' => 'required'
        ]);

        $catalog = $this->catalogs->find($request->input('id'));
        $this->catalogs->delete($catalog);

        return response()->json(["status" => 'OK'],200);
    }


}