<?php

namespace App\Infrastructure\Catalog;


use App\Infrastructure\DoctrineBaseRepository;
use App\Repositories\CatalogRepository;

class DoctrineCatalogRepository extends DoctrineBaseRepository  implements CatalogRepository
{

}