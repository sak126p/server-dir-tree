<?php

namespace App\Providers;

use App\Entities\Catalog;
use App\Infrastructure\Catalog\DoctrineCatalogRepository;
use App\Repositories\CatalogRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CatalogRepository::class, function($app) {
            // This is what Doctrine's EntityRepository needs in its constructor.
            return new DoctrineCatalogRepository(
                $app['em'],
                $app['em']->getClassMetaData(Catalog::class)
            );
        });
    }
}
