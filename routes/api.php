<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['as' => 'Catalog::','prefix' => '/catalog'], function () {

    Route::get('/getall', 'CatalogController@index');
    Route::resource('/add', 'CatalogController');
    Route::post('/update', 'CatalogController@update');
    Route::post('/delete', 'CatalogController@destroy');

});
