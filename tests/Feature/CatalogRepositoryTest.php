<?php

namespace Tests\Feature;

use App\Entities\Catalog;
use App\Repositories\CatalogRepository;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class CatalogRepositoryTest extends TestCase
{
    protected $repository;

    protected static $catalog;

    public function setUp()
    {
        parent::setUp();

        $this->repository = App::make(CatalogRepository::class);
    }

    public function testCreateAndSave()
    {
        $data = array(
            'name' => 'Root node',
        );

        $catalog = $this->repository->create($data);

        self::$catalog = $this->repository->save($catalog);

        $this->assertDatabaseHas('catalogs',['id'=>self::$catalog->getId()]);
    }

    public function testUpdateAndSave()
    {
        $data = array(
            'name' => 'Root node 1'
        );

        $catalog = $this->repository->update($data, self::$catalog->getId());

        self::$catalog = $this->repository->save($catalog);

        $this->assertEquals($data['name'],self::$catalog->getName());
    }

    public function testFindAll()
    {
        $catalog = $this->repository->findAll();

        $this->assertContainsOnlyInstancesOf(Catalog::class, $catalog);
    }

    public function testDelete()
    {
        $catalog = $this->repository->find(self::$catalog->getId());

        $result = $this->repository->delete($catalog);

        $this->assertTrue($result);
    }
}
